---
Week: 16
Content:  Project part 2 - phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 16 Project part 2 startup

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Project details ready for ww17

### Learning goals


## Deliverable

* Decide on details for part 2 of the project - see ww17 for details. 


## Comments