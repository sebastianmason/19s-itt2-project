---
Week: 19
Content: Project part 2 phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 19

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.


### Learning goals
None from the teachers at this time.

## Deliverables

* OLA23 must be handed in during the week. See [this doc](https://eal-itt.gitlab.io/19s-itt2-project/OLA23.pdf)
* all groups have added a `groups.yml` file and made a MR to the [admin project](https://gitlab.com/EAL-ITT/19s-itt2-project).

    Go [here](https://eal-itt.gitlab.io/19s-itt2-project/grouplist2.pdf) for details
    
    
## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session

* 9:30? You work

    Group morning meeting
    
        You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
        Ordinary agenda:
        1. (5 min) Round the table: What did I do, and what did I finish?
        2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
        3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  

* 10:30 Project experience exchange
    
    see week 18 for details.


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.


## Comments

* We only have one day this week. Tuesday has been moved to next week, where we will have 3 days.
* There will not be teacher meetings this week