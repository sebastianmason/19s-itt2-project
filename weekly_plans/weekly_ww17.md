---
Week: 17
Content:  Project part 2 - phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 17 Project part 2 startup

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* project plan for part 2 of the project - use the same phases as part 1
* new gitlab project
* pre mortem meeting completed and included in project plan
* project phases created as milestones in gitlab project

### Learning goals

No specific goals for this week


## Deliverable

* Complete project plan - include goals and deliverables. The teachers have supplied a project plan with missing parts.
* All groups have performed a pre-mortem meeting and included minutes of meeting in project plan
* All groups have created a new gitlab project.
* Project phases has been created as milestones in gitlab (deliverable issues HAS to be assigned to a milestone)
* Project presentation

## Schedule

Tuesday

* 8:15 Introduction to the project by NISI and MON

    We will go through the expectations and discuss the project and the plans.
    
    Rundown of project part 2 phases, projectplan, deliverables and Gitlab.

* 9:00 Exercise 0: Pre-mortem meeting

* 9:30 Exercise 1: Project planning

* 10:00 Week 15 presentations

    All groups presents what they have been doing in week 15. 
    Presentation duration 10 minutes.

* 12:15 Continue project planning

* 13:00 Presentations in network lab

    Each group presents their project plan and gitlab project to MON and NISI. Everyone else is welcome to attend the presentations.
    Presentation duration 10 minutes

Thursday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Idea generation - Weekly workshops

    We, the lecturers, would like to give workshops, lectures etc. every week. But we need your input on the topics.

* 9:30 (ish) Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

## Hands-on time


## Comments
* We do not have teacher meetings this week - we couldn't find the time. In ww18, we will discuss your phase 1 project plan evaluation at the teacher meeting.
* Regarding the discussion on working with 230V AC. It is really really not allowed. See e.g. [here (Danish)](https://www.sik.dk/privat/gor-det-sikkert/el/gor-det-selv/hvad-ma-du-lave-uden-autorisation)