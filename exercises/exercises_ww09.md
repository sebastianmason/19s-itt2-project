# Exercises for ww09


## Exercise 1 - Setting up a virtual machine with a webserver

1. Check your notes from december about installing a webserver.

    We used [this](https://www.rosehosting.com/blog/how-to-install-and-configure-nginx-on-debian-9/) guide.
    
2. Get access to a virtualmachine

    MON will give you IP, username+pass and such
    
3. Install nginx

4. Add the [example website](https://gitlab.com/npes/19s-itt2-dashboard-example) and make it point to your raspberry

5. The same rules as for the raspberry applies. See exercise 2 in week 7

6. create the reinstallation script


## Optional exercise - use nginx on raspberry

This will be mandatory at a later time.

follow [this](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-14-04)

