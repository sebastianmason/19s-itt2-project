# Exercises for ww11

## Exercise 1 - Status LED extension

In this exercise you will extend the minimum system with another led. The led will be used to show status messages locally on the datalogger device (RPi + Atmega328).

This is very useful if the datalogger is located in a remote place where the user does not have access to the dashboard.

Another reason is that there might be errors, on the datalogger, that are not relayed to the dashboard due to lost connection etc.

The led can show different status messages, by varying the amount of times it blinks, and the duration of the blinking.

1. Make a document called `status_protocol.md` in you gitlab project.
2. Make a list of statuses that you will show on the led. Decide for each status if it should be updated from the API/Dashboard or directly from the ATMega328. `Status examples:` message received from UART, message sent successfully to UART, failed to send message x times etc.
3. For each status describe the number of times and duration of blinking.
4. Add another led to the temperature sensor board. Chose a different color than the one you are already using.
5. Make a function in the C code that accepts blinking times and duration as arguments. Something like  `void statusLed (times, duration)`
6. Use the function to implement the statuses described in `status_protocol.md`

### Notice! 
You can use the blocking delay from `avr/delay.h` to blink the led, or you can use a timer to make a non blocking delay. If you use a blocking delay your system will be unresponsive in the delay periods! This [video](https://youtu.be/cAui6116XKc) shows how to use timers on ATmega328. Link to code examples from the video is [here](https://www.dropbox.com/sh/7qdeb6n8rsm0li8/AABRwn3Huw-udGVXL_4ai-TIa).

### Extra coolness
Instead of just turning the led on and off you can dim it up and down by using PWM, it looks a lot cooler!. This is not easy if you want to do it in a non blocking fashion.
If you want to know more about PWM on ATmega328 you can read about it in the datasheet. This [video](https://youtu.be/ZhIRRyhfhLM) shows the basics of PWM on ATmega328. Link to code examples from the video is [here](https://www.dropbox.com/sh/7qdeb6n8rsm0li8/AABRwn3Huw-udGVXL_4ai-TIa).

## Exercise 2 - Datalogger housing

In week 12 and 13 you are going to fablab for workshops in laser cutting and 3D printing.
Before you do that we would like you to have a sketch for housing the Raspberry Pi + ATmega328 + tempsensor + led's.
The components should be in the same housing with exposed leds, temp sensor, connectors etc.

1. Create a sketch that shows how you want the finished housing to look like.
2. Save the sketch as housing.pdf in your gitlab project
3. Bring the sketch to the workshop, to be able to discuss with the fablab staff on how to build it the best way.

### Extra
1. Make a document that explains the value your housing design adds to the usability.
2. Test your housing idea by asking potential users about your design. Prepare 3-5 questions to ask. The more users you ask the more useful your design will be. 
3. Correct your design according to user response.

